def parenthesisCheck(string):
    counter = 0
    for i in string :
        if(i == "("):
            counter += 1
        elif(i == ")"):
            counter -= 1

        if(counter < 0):
            return False

    return counter == 0

def parenthesisCheckF(string):
    closedCounter = 0
    opnedCounter = 0
    for i in string :
        if(i == "("):
            opnedCounter += 1
        elif(i == ")"):
            closedCounter += 1

        if(opnedCounter < closedCounter):
            return False

    return opnedCounter == closedCounter

print(parenthesisCheckF("(()()( jkdncd))"))
